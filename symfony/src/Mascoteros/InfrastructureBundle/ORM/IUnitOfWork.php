<?php

namespace Mascoteros\InfrastructureBundle\ORM;

interface IUnitOfWork
{
	public function commit();
}
