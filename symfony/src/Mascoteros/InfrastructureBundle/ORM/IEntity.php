<?php

namespace Mascoteros\InfrastructureBundle\ORM;

interface IEntity
{
	public function getId();
}
