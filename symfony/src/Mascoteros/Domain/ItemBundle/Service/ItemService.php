<?php

namespace Mascoteros\Domain\ItemBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\Domain\ItemBundle\Entity\Item;
use Mascoteros\Domain\ItemBundle\Exception\DomainException;
use Mascoteros\Domain\ItemBundle\Specification\Item\EanSpecification;
use Mascoteros\InfrastructureBundle\ORM\IRepository;
use Mascoteros\InfrastructureBundle\ORM\IUnitOfWork;

class ItemService
{
	/**
	 * @var IRepository
	 */
	private $itemRepository;

	/**
	 * @var IUnitOfWork
	 */
	private $uow;

	public function __construct(IUnitOfWork $uow, IRepository $itemRepository)
	{
		$this->uow = $uow;
		$this->itemRepository = $itemRepository;
	}

	/**
	 * Returns item by id
	 *
	 * @param int $id
	 * @return Item
	 */
	public function getItem(int $id) : ?Item
	{
		return $this->itemRepository->findById($id);
	}

    /**
     * Returns item by ean
     *
     * @param int $ean
     * @return Item
     */
    public function findItemByEan(int $ean) : ?Item
    {
		return $this->itemRepository->findBySpecification(new EanSpecification($ean));
	}

	/**
	 * Returns all items
	 *
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getAllItems() : ArrayCollection
	{
		return $this->itemRepository->findAll();
	}

	/**
	 * Create item
	 *
	 * @param Feed $feed
	 * @param string $title
	 * @param string $description
	 * @param string $link
	 * @param string $image_link
	 * @param float $price
	 * @param float $reduced_price
	 * @param integer $ean
	 * @return Item
	 */
	public function create(Feed $feed,string $title,string $description,string $link,string $image_link,float $price,float $reduced_price,int $ean) : Item
	{
		$item = new Item($feed,$title,$description,$link,$image_link,$price,$reduced_price,$ean);

		$this->itemRepository->add($item);
		$this->uow->commit();

		return $item;
	}

    /**
     * Update item
     *
     * @param Item $item
     * @return Item
     */
    public function update(Item $item) : Item
    {
        $this->itemRepository->update($item);
        $this->uow->commit();

        return $item;
    }

	/**
	 * Remove item
	 *
	 * @param int $id
	 * @throws DomainException
	 */
	public function remove(int $id)
	{
        $item = $this->getItem($id);

		if ($item === null) {
			throw new DomainException(sprintf('Item "%s" does not exist', $id));
		}

        $item->getFeed()->removeItem($item);

		$this->itemRepository->remove($item);
		$this->uow->commit();
	}
}
