<?php

namespace Mascoteros\Domain\ItemBundle\Doctrine;

use Mascoteros\InfrastructureBundle\Doctrine\UnitOfWork;
use Mascoteros\InfrastructureBundle\ORM\IRepository;
use Mascoteros\InfrastructureBundle\ORM\IUnitOfWork;
use Doctrine\Bundle\DoctrineBundle\Registry;

class DoctrineFactory implements IRepositoryFactory, IUnitOfWorkFactory
{
	private $doctrine;

	/**
	 * @param Registry $doctrine
	 */
	public function __construct(Registry $doctrine)
	{
		$this->doctrine = $doctrine;
	}

    /**
     * @return IRepository
     */
    public function createItemRepository() : IRepository
    {
        return $this->doctrine->getRepository('MascoterosDomainItemBundle:Item');
    }

	/**
	 * @return IUnitOfWork
	 */
	public function createUnitOfWork() : IUnitOfWork
	{
		return new UnitOfWork($this->doctrine);
	}
}
