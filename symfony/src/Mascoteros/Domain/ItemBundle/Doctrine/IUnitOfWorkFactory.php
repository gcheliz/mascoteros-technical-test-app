<?php

namespace Mascoteros\Domain\ItemBundle\Doctrine;

use Mascoteros\InfrastructureBundle\ORM\IUnitOfWork;

interface IUnitOfWorkFactory
{
	/**
	 * @return IUnitOfWork
	 */
	public function createUnitOfWork() : IUnitOfWork;
}
