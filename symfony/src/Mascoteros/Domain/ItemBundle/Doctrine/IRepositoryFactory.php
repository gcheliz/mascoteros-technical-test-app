<?php

namespace Mascoteros\Domain\ItemBundle\Doctrine;

use Mascoteros\InfrastructureBundle\ORM\IRepository;

interface IRepositoryFactory
{
    /**
     * @return IRepository
     */
    public function createItemRepository() : IRepository;
}
