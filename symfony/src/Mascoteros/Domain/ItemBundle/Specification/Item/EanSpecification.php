<?php
/**
 * Created by PhpStorm.
 * User: Gonzalo
 * Date: 08/06/2017
 * Time: 23:32
 */

namespace Mascoteros\Domain\ItemBundle\Specification\Item;

use Doctrine\Common\Collections\Criteria;
use Mascoteros\Domain\ItemBundle\Entity\Item;
use Mascoteros\InfrastructureBundle\ORM\IEntity;
use Mascoteros\InfrastructureBundle\ORM\ISpecification;
use Mascoteros\InfrastructureBundle\ORM\ISpecificationCriteria;

class EanSpecification implements ISpecification, ISpecificationCriteria
{
    private $ean;

    public function __construct(int $ean)
    {
        $this->ean = $ean;
    }

    public function isSatisfiedBy(IEntity $object) : bool
    {
        if (!$object instanceof Item) {
            throw new \BadMethodCallException(sprintf("I only deal with items, you gave me: %s", get_class($object)));
        }

        return $object->getEan() == $this->ean;
    }

    public function getCriteria() : Criteria
    {
        return Criteria::create()->where(Criteria::expr()->eq('ean', $this->ean));
    }
}