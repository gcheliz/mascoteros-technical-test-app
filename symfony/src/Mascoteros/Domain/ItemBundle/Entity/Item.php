<?php

namespace Mascoteros\Domain\ItemBundle\Entity;

use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\InfrastructureBundle\ORM\IEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="item")
 * @ORM\HasLifecycleCallbacks()
 */
class Item implements IEntity
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=64)
	 * @var string
	 */
	private $title;

	/**
	 * @ORM\Column(type="string", length=64)
	 * @var string
	 */
	private $link;

    /**
     * @ORM\Column(type="string", length=64)
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=64)
     * @var string
     */
    private $imagelink;

    /**
     * @ORM\Column(type="float")
     * @var float
     */
    private $price;

    /**
     * @ORM\Column(type="float")
     * @var float
     */
    private $reduced_price;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $ean;

    /**
     * @ORM\ManyToOne(targetEntity="Mascoteros\Domain\FeedBundle\Entity\Feed", inversedBy="items")
     * @var Feed
     */
    private $feed;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updated;

	public function __construct(Feed $feed,$title,$description,$link,$image_link,$price,$reduced_price,$ean)
	{
		$this->setFeed($feed)
            ->setTitle($title)
            ->setDescription($description)
            ->setLink($link)
            ->setImagelink($image_link)
            ->setPrice($price)
            ->setReducedPrice($reduced_price)
            ->setEan($ean)
            ->setCreated(new \DateTime());
	}

	public function getId() : int
	{
		return $this->id;
	}

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Item
     */
    public function setTitle(string $title) : Item
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Item
     */
    public function setLink(string $link) : Item
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink() : string
    {
        return $this->link;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Item
     */
    public function setDescription(string $description) : Item
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * Set imagelink
     *
     * @param string $imagelink
     *
     * @return Item
     */
    public function setImagelink(string $imagelink) : Item
    {
        $this->imagelink = $imagelink;

        return $this;
    }

    /**
     * Get imagelink
     *
     * @return string
     */
    public function getImagelink() : string
    {
        return $this->imagelink;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Item
     */
    public function setPrice(float $price) : Item
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice() : float
    {
        return $this->price;
    }

    /**
     * Set reducedPrice
     *
     * @param float $reducedPrice
     *
     * @return Item
     */
    public function setReducedPrice(float $reducedPrice) : Item
    {
        $this->reduced_price = $reducedPrice;

        return $this;
    }

    /**
     * Get reducedPrice
     *
     * @return float
     */
    public function getReducedPrice() : float
    {
        return $this->reduced_price;
    }

    /**
     * Set ean
     *
     * @param int $ean
     *
     * @return Item
     */
    public function setEan(int $ean) : Item
    {
        $this->ean = $ean;

        return $this;
    }

    /**
     * Get ean
     *
     * @return int
     */
    public function getEan() : int
    {
        return $this->ean;
    }

    /**
     * Set feed
     *
     * @param Feed $feed
     *
     * @return Item
     */
    public function setFeed(Feed $feed = null) : Item
    {
        $this->feed = $feed;
        $this->feed->addItem($this);

        return $this;
    }

    /**
     * Get feed
     *
     * @return Feed
     */
    public function getFeed() : Feed
    {
        return $this->feed;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Item
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Item
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime() {
        $this->setUpdated(new \DateTime());
    }
}
