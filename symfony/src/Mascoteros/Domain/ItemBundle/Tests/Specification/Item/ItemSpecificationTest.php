<?php

namespace Mascoteros\Domain\ItemBundle\Tests\Specification;

use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\Domain\ItemBundle\Entity\Item;
use Mascoteros\Domain\ItemBundle\Specification\Item\EanSpecification;
use Mascoteros\Domain\ItemBundle\Tests\IntegrationTestCaseBase;

class ItemSpecificationTest extends IntegrationTestCaseBase
{
	public function testSatisfiedIsShouldBeTrue()
	{
		$specification = new EanSpecification(1);
		$this->assertTrue($specification->isSatisfiedBy(new Item(new Feed('test Feed','http://example.com'), 'Text of item','Text of description','http://example.com/item','http://example.com/image/image-link',32,25,1)));
	}

	public function testSatisfiedIsShouldBeFalse()
	{
		$specification = new EanSpecification(1);
		$this->assertFalse($specification->isSatisfiedBy(new Item(new Feed('test Feed','http://example.com'), 'Text of item 2','Text of description 2','http://example.com/item/2','http://example.com/image/image-link-2',20,15,2)));
	}
}
