<?php

namespace Mascoteros\Domain\ItemBundle\Tests\DataFixtures;

use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\Domain\ItemBundle\Entity\Item;
use Mascoteros\InfrastructureBundle\Doctrine\DoctrineHelper;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadItemData implements FixtureInterface
{
	/**
	 * Load data fixtures with the passed EntityManager
	 *
	 * @param \Doctrine\Common\Persistence\ObjectManager $manager
	 * @return void
	 */
	function load(ObjectManager $manager)
	{
		DoctrineHelper::truncate($manager, 'Mascoteros\Domain\ItemBundle\Entity\Item');

		$feed = new Feed('Feed Test', 'http://example.com/test');
        new Item($feed, 'Text of item 1','Text of description 1','http://example.com/item/1','http://example.com/image/image-link-1',32,25,1);
        new Item($feed, 'Text of item 2','Text of description 2','http://example.com/item/2','http://example.com/image/image-link-2',32,25,2);

		$manager->persist($feed);
		$manager->flush();
	}
}
