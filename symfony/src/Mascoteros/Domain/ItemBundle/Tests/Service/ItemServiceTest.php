<?php

namespace Mascoteros\Domain\ItemBundle\Tests\Service;

use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\Domain\ItemBundle\Entity\Item;
use Mascoteros\Domain\ItemBundle\Service\ItemService;
use Mascoteros\Domain\ItemBundle\Tests\TestCaseBase;
use Mascoteros\Domain\ItemBundle\Tests\Service\FakeRepository;

class ItemServiceTest extends TestCaseBase
{
	public function testCreate()
	{
		// arrange
		$service = $this->createItemService();
		$feed = new Feed('Test Feed', 'http://example.com');
		// act
		$item = $service->create($feed, 'Text of item','Text of description','http://example.com/item','http://example.com/image/image-link',32,25,1);
		// assert
		$this->assertInstanceOf('Mascoteros\Domain\ItemBundle\Entity\Item', $item);
		$this->assertEquals('Text of item', $item->getTitle());
		$this->assertEquals('Text of description', $item->getDescription());
		$this->assertEquals('http://example.com/item', $item->getLink());
		$this->assertEquals('http://example.com/image/image-link', $item->getImagelink());
		$this->assertEquals(32, $item->getPrice());
		$this->assertEquals(25, $item->getReducedPrice());
		$this->assertSame($feed, $item->getFeed());
	}

	public function testRemove()
	{
		// arrange
		$service = $this->createItemService();
		$feed = $service->getItem(1)->getFeed();
		// act
		$service->remove(1);
		// assert
		$this->assertEquals(0, $feed->getItems()->count());
	}

	public function testRemoveNonexistentShouldThrowException()
	{
		$this->expectException('Mascoteros\Domain\ItemBundle\Exception\DomainException');
		// arrange
		$service = $this->createItemService();
		// act
		$service->remove(100500);
	}

	public function testGetAllItems()
	{
		// arrange
		$service = $this->createItemService();
		// act
        $items = $service->getAllItems();
		// assert
		$this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $items);
		$this->assertContainsOnlyInstancesOf('Mascoteros\Domain\ItemBundle\Entity\Item', $items->getValues());
	}

	public function testGetItemById()
	{
		// arrange
		$service = $this->createItemService();
		// act
        $item = $service->getItem(1);
		// assert
		$this->assertInstanceOf('Mascoteros\Domain\ItemBundle\Entity\Item', $item);
		$this->assertEquals(1, $item->getId());
	}

	private function createItemService() : ItemService
	{
		$unitOfWork = $this->getMockForAbstractClass('Mascoteros\InfrastructureBundle\ORM\IUnitOfWork');
		$repository = new FakeRepository('Mascoteros\Domain\ItemBundle\Entity\Item', array(
			$this->createItem('Text of item 1','Text of description 1','http://example.com/item/1','http://example.com/image/image-link-1',32,25,2),
			$this->createItem('Text of item 2','Text of description 2','http://example.com/item/2','http://example.com/image/image-link-2',15,10,3),
			$this->createItem('Text of item 3','Text of description 3','http://example.com/item/3','http://example.com/image/image-link-3',40,20,4),
		));
		return new ItemService($unitOfWork, $repository);
	}

	private function createItem(string $title,string $description,string $link,string $image_link,float $price,float $reduced_price,int $ean) : Item
	{
		return new Item(new Feed('Test Feed 1', 'http://example.com'), $title, $description, $link, $image_link, $price, $reduced_price, $ean);
	}
}
