<?php

namespace Mascoteros\Domain\ItemBundle\Tests\Entity;

use Mascoteros\Domain\ItemBundle\Entity\Item;
use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\Domain\ItemBundle\Tests\TestCaseBase;

class ItemTest extends TestCaseBase
{
	public function testConstruct()
	{
		// arrange
		$feed = $this->createFeed();
		// act
        $item = new Item($feed, 'Text of item','Text of description','http://example.com/item','http://example.com/image/image-link',32,25,1);
        // assert
        $this->assertInstanceOf('Mascoteros\Domain\ItemBundle\Entity\Item', $item);
        $this->assertEquals('Text of item', $item->getTitle());
        $this->assertEquals('Text of description', $item->getDescription());
        $this->assertEquals('http://example.com/item', $item->getLink());
        $this->assertEquals('http://example.com/image/image-link', $item->getImagelink());
        $this->assertEquals(32, $item->getPrice());
        $this->assertEquals(25, $item->getReducedPrice());
        $this->assertSame($feed, $item->getFeed());
	}

	private function createFeed()
	{
		return new Feed('Feed Test', 'http://example.com');
	}
}
