<?php

namespace Mascoteros\Domain\ItemBundle\Tests\Integration;

use Mascoteros\Domain\ItemBundle\Tests\IntegrationTestCaseBase;

class ContainerTest extends IntegrationTestCaseBase
{
	public function testContains_ItemService()
	{
		$this->assertInstanceOf('Mascoteros\Domain\ItemBundle\Service\ItemService', $this->get('mascoteros.domain.service.item'));
	}
}
