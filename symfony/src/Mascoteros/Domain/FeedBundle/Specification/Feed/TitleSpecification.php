<?php
/**
 * Created by PhpStorm.
 * User: Gonzalo
 * Date: 09/06/2017
 * Time: 1:05
 */

namespace Mascoteros\Domain\FeedBundle\Specification\Feed;


use Doctrine\Common\Collections\Criteria;
use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\InfrastructureBundle\ORM\IEntity;
use Mascoteros\InfrastructureBundle\ORM\ISpecification;
use Mascoteros\InfrastructureBundle\ORM\ISpecificationCriteria;

class TitleSpecification implements ISpecification, ISpecificationCriteria
{
    private $title;

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function isSatisfiedBy(IEntity $object) : bool
    {
        if (!$object instanceof Feed) {
            throw new \BadMethodCallException(sprintf("I only deal with feeds, you gave me: %s", get_class($object)));
        }

        return $object->getTitle() == $this->title;
    }

    public function getCriteria() : Criteria
    {
        return Criteria::create()->where(Criteria::expr()->eq('title', $this->title));
    }
}