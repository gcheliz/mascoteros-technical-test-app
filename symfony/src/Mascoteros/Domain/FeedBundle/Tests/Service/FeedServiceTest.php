<?php

namespace Mascoteros\Domain\FeedBundle\Tests\Service;

use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\Domain\FeedBundle\Service\FeedService;
use Mascoteros\Domain\FeedBundle\Tests\TestCaseBase;
use Mascoteros\Domain\FeedBundle\Tests\Service\FakeRepository;

class FeedServiceTest extends TestCaseBase
{
	public function testCreate()
	{
		// arrange
		$service = $this->createFeedService();
		// act
		$feed = $service->create('Test Feed','http://example.com');
		// assert
		$this->assertInstanceOf('Mascoteros\Domain\FeedBundle\Entity\Feed', $feed);
		$this->assertEquals('Test Feed', $feed->getTitle());
		$this->assertEquals('http://example.com', $feed->getLink());
	}

	public function testRemove()
	{
		// arrange
		$service = $this->createFeedService();
		$feed = $service->getFeed(1);
		// act
		$service->remove(1);
		// assert
		$this->assertEquals(null, $service->getFeed(1));
	}

	public function testRemoveNonexistentShouldThrowException()
	{
		$this->expectException('Mascoteros\Domain\FeedBundle\Exception\DomainException');
		// arrange
		$service = $this->createFeedService();
		// act
		$service->remove(100500);
	}

	public function testGetAllFeeds()
	{
		// arrange
		$service = $this->createFeedService();
		// act
        $feeds = $service->getAllFeeds();
		// assert
		$this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $feeds);
		$this->assertContainsOnlyInstancesOf('Mascoteros\Domain\FeedBundle\Entity\Feed', $feeds->getValues());
	}

	public function testGetFeedById()
	{
		// arrange
		$service = $this->createFeedService();
		// act
        $feed= $service->getFeed(1);
		// assert
		$this->assertInstanceOf('Mascoteros\Domain\FeedBundle\Entity\Feed', $feed);
		$this->assertEquals(1, $feed->getId());
	}

	private function createFeedService() : FeedService
	{
		$unitOfWork = $this->getMockForAbstractClass('Mascoteros\InfrastructureBundle\ORM\IUnitOfWork');
		$repository = new FakeRepository('Mascoteros\Domain\FeedBundle\Entity\Feed', array(
			$this->createFeed('Text of Feed 1','http://example.com/1'),
			$this->createFeed('Text of Feed 2','http://example.com/2'),
			$this->createFeed('Text of Feed 3','http://example.com/3'),
		));
		return new FeedService($unitOfWork, $repository);
	}

	private function createFeed(string $title,string $link) : Feed
	{
		return new Feed($title, $link);
	}
}
