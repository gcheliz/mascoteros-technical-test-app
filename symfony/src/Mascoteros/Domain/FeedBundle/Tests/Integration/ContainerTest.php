<?php

namespace Mascoteros\Domain\FeedBundle\Tests\Integration;

use Mascoteros\Domain\FeedBundle\Tests\IntegrationTestCaseBase;

class ContainerTest extends IntegrationTestCaseBase
{
	public function testContains_FeedService()
	{
		$this->assertInstanceOf('Mascoteros\Domain\FeedBundle\Service\FeedService', $this->get('mascoteros.domain.service.feed'));
	}
}
