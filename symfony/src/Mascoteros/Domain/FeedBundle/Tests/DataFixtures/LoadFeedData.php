<?php

namespace Mascoteros\Domain\FeedBundle\Tests\DataFixtures;

use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\InfrastructureBundle\Doctrine\DoctrineHelper;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFeedData implements FixtureInterface
{
	/**
	 * Load data fixtures with the passed EntityManager
	 *
	 * @param \Doctrine\Common\Persistence\ObjectManager $manager
	 * @return void
	 */
	function load(ObjectManager $manager)
	{
		DoctrineHelper::truncate($manager, 'Mascoteros\Domain\FeedBundle\Entity\Feed');

		$feed = new Feed('Feed Test', 'http://example.com');

		$manager->persist($feed);
		$manager->flush();
	}
}
