<?php

namespace Mascoteros\Domain\FeedBundle\Tests\Specification;

use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\Domain\FeedBundle\Specification\Feed\TitleSpecification;
use Mascoteros\Domain\ItemBundle\Tests\IntegrationTestCaseBase;

class FeedSpecificationTest extends IntegrationTestCaseBase
{
	public function testSatisfiedIsShouldBeTrue()
	{
		$specification = new TitleSpecification('Feed Test');
		$this->assertTrue($specification->isSatisfiedBy(new Feed('Feed Test','http://example.com')));
	}

	public function testSatisfiedIsShouldBeFalse()
	{
		$specification = new TitleSpecification('Feed Test2');
		$this->assertFalse($specification->isSatisfiedBy(new Feed('test Feed','http://example.com')));
	}
}
