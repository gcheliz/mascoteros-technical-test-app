<?php

namespace Mascoteros\Domain\FeedBundle\Tests\Entity;

use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\Domain\FeedBundle\Tests\TestCaseBase;

class FeedTest extends TestCaseBase
{
	public function testConstruct()
	{
		// act
        $feed = new Feed('Feed Test', 'http://example.com');
        // assert
        $this->assertInstanceOf('Mascoteros\Domain\FeedBundle\Entity\Feed', $feed);
        $this->assertEquals('Feed Test', $feed->getTitle());
        $this->assertEquals('http://example.com', $feed->getLink());
	}
}
