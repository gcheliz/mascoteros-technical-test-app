<?php

namespace Mascoteros\Domain\FeedBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Mascoteros\Domain\FeedBundle\Exception\DomainException;
use Mascoteros\Domain\ItemBundle\Entity\Item;
use Mascoteros\InfrastructureBundle\ORM\IEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="feed")
 * @ORM\HasLifecycleCallbacks()
 */
class Feed implements IEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @var int
	 */
	private $id;

    /**
     * @ORM\Column(type="string", length=64)
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=120)
     * @var string
     */
    private $link;

    /**
     * @ORM\OneToMany(targetEntity="Mascoteros\Domain\ItemBundle\Entity\Item", mappedBy="feed", cascade={"persist", "remove"})
     * @var ArrayCollection|PersistentCollection|Item[]
     */
    private $items;

	/**
     * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	private $created;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updated;

	public function __construct(string $title,string $link)
	{
		$this->setTitle($title)
            ->setLink($link)
			->setCreated(new \DateTime());
		$this->items = new ArrayCollection();
	}

	public function getId() : int
	{
		return $this->id;
	}

	public function getTitle() : string
	{
		return $this->title;
	}

	public function setTitle(string $title) : Feed
	{
		$this->title = $title;
		return $this;
	}

	public function getCreated() : \DateTime
	{
		return $this->created;
	}

	public function setCreated(\DateTime $created) : Feed
	{
		$this->created = $created;
		return $this;
	}

    public function addItem(Item $item)
    {
        if ($this->hasItem($item)) {
            throw new DomainException('Item already added');
        }
        $this->items->add($item);
    }

    public function removeItem(Item $item)
    {
        if (!$this->hasItem($item)) {
            throw new DomainException('Item not exists');
        }
        $this->items->removeElement($item);
    }

    public function hasItem(Item $item) : bool
    {
        return $this->items->contains($item);
    }

    /**
     * Get items
     *
     * @return ArrayCollection
     */
    public function getItems() : ArrayCollection
    {
        return $this->items;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Feed
     */
    public function setLink($link) : Feed
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink() : string
    {
        return $this->link;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Feed
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime()
    {
        $this->setUpdated(new \DateTime());
    }
}
