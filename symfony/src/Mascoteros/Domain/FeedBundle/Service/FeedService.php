<?php

namespace Mascoteros\Domain\FeedBundle\Service;

use Mascoteros\Domain\FeedBundle\Entity\Feed;
use Mascoteros\Domain\FeedBundle\Exception\DomainException;
use Mascoteros\Domain\FeedBundle\Specification\Feed\TitleSpecification;
use Mascoteros\InfrastructureBundle\ORM\IRepository;
use Mascoteros\InfrastructureBundle\ORM\IUnitOfWork;
use Doctrine\Common\Collections\ArrayCollection;

class FeedService
{
	/**
	 * @var IRepository
	 */
	private $feedRepository;

	/**
	 * @var IUnitOfWork
	 */
	private $uow;

	public function __construct(IUnitOfWork $uow, IRepository $feedRepository)
	{
		$this->uow = $uow;
		$this->feedRepository = $feedRepository;
	}

	/**
	 * Returns feed by id
	 *
	 * @param int $id
	 * @return Feed
	 */
	public function getFeed(int $id) : ?Feed
	{
		return $this->feedRepository->findById($id);
	}

    /**
     * Returns feed by title
     *
     * @param string $title
     * @return Feed
     */
    public function findFeedByTitle(string $title) : ?Feed
    {
        return $this->feedRepository->findBySpecification(new TitleSpecification($title));
    }

	/**
	 * Returns all feeds
	 *
	 * @return ArrayCollection
	 */
	public function getAllFeeds() : ArrayCollection
	{
		return $this->feedRepository->findAll();
	}

	/**
	 * Create feed
	 *
	 * @param string $title
	 * @param string $text
	 * @return Feed
	 */
	public function create(string $title,string $text) : Feed
	{
		$feed = new Feed($title, $text);

		$this->feedRepository->add($feed);
		$this->uow->commit();

		return $feed;
	}

	/**
	 * Remove feed
	 *
	 * @param int $id
	 * @throws DomainException
	 */
	public function remove(int $id)
	{
        $feed = $this->getFeed($id);

		if ($feed === null) {
			throw new DomainException(sprintf('Feed "%s" does not exist', $id));
		}

		$this->feedRepository->remove($feed);
		$this->uow->commit();
	}
}
