<?php

namespace Mascoteros\Domain\FeedBundle\Doctrine;

use Mascoteros\InfrastructureBundle\ORM\IUnitOfWork;

interface IUnitOfWorkFactory
{
	/**
	 * @return IUnitOfWork
	 */
	public function createUnitOfWork() : IUnitOfWork;
}
