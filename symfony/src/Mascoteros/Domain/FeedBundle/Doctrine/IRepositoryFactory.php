<?php

namespace Mascoteros\Domain\FeedBundle\Doctrine;

use Mascoteros\InfrastructureBundle\ORM\IRepository;

interface IRepositoryFactory
{
	/**
	 * @return IRepository
	 */
	public function createFeedRepository() : IRepository;

}
