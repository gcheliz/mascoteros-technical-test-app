<?php

namespace Mascoteros\Domain\FeedBundle\Doctrine;

use Mascoteros\InfrastructureBundle\Doctrine\UnitOfWork;
use Mascoteros\InfrastructureBundle\ORM\IRepository;
use Mascoteros\InfrastructureBundle\ORM\IUnitOfWork;
use Doctrine\Bundle\DoctrineBundle\Registry;

class DoctrineFactory implements IRepositoryFactory, IUnitOfWorkFactory
{
	private $doctrine;

	/**
	 * @param Registry $doctrine
	 */
	public function __construct(Registry $doctrine)
	{
		$this->doctrine = $doctrine;
	}

	/**
	 * @return IRepository
	 */
	public function createFeedRepository() : IRepository
	{
		return $this->doctrine->getRepository('MascoterosDomainFeedBundle:Feed');
	}

	/**
	 * @return IUnitOfWork
	 */
	public function createUnitOfWork() : IUnitOfWork
	{
		return new UnitOfWork($this->doctrine);
	}
}
