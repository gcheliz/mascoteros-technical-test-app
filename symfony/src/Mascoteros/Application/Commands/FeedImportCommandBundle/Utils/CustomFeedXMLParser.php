<?php

namespace Mascoteros\Application\Commands\FeedImportCommandBundle\Utils;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Exception\IOException;

/**
 * Created by PhpStorm.
 * User: ud-dev
 * Date: 12/06/17
 * Time: 10:56
 */
class CustomFeedXMLParser
{
    private $xml;

    public function __construct(string $type)
    {
        $path = __DIR__.'/../Resources/public/'.$type.'.xml';
        $this->xml = $this->loadXMLFile($path);
    }
    
    private function loadXMLFile(string $path)
    {
        if(file_exists($path))
        {
            return simplexml_load_file($path);
        }
        else
        {
            throw new FileNotFoundException('The xml file '.$path.' doesn\'t exist!');  
        }
    }

    public function getXMLNodeValues(string $node) : array
    {
        try
        {
            $node_values = $this->xml->xpath('//'.$node);
        }
        catch (IOException $exception)
        {
            throw new IOException('The XML File can\'t be readed');
        }
        return $this->convertSimpleXMLElementToArray($node_values);
    }

    public function loadProductFeed() : array
    {
        return $this->getXMLNodeValues('channel');
    }

    public function loadItemsFeed() : array
    {
        return $this->getXMLNodeValues('item');
    }

    public function loadItemsExtraInfoFeed() : array
    {
        $items = [];

        $items['image_link'] = $this->loadItemImageLinkFeed();
        $items['price'] = $this->loadItemPriceFeed();
        $items['reduced_price'] = $this->loadItemReducedPriceFeed();
        $items['ean'] = $this->loadItemEanFeed();

        return $items;
    }

    public function loadItemPriceFeed() : array
    {
        return $this->getXMLNodeValues('g:price');
    }

    public function loadItemImageLinkFeed() : array
    {
        return $this->getXMLNodeValues('g:image_link');
    }

    public function loadItemReducedPriceFeed() : array
    {
        return $this->getXMLNodeValues('g:reduced_price');
    }

    public function loadItemEanFeed() : array
    {
        return $this->getXMLNodeValues('g:ean');
    }

    public function convertSimpleXMLElementToArray($xmlObject, $out = array ()) : array
    {
        foreach ( (array) $xmlObject as $index => $node )
            $out[$index] = ( is_object ( $node ) ) ? $this->convertSimpleXMLElementToArray ( $node ) : $node;

        return $out;
    }
}
