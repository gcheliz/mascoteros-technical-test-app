<?php
/**
 * Created by PhpStorm.
 * User: Gonzalo
 * Date: 06/06/2017
 * Time: 23:02
 */

namespace Mascoteros\Application\Commands\FeedImportCommandBundle\Command;

use Mascoteros\Domain\FeedBundle\Service\FeedService;
use Mascoteros\Domain\ItemBundle\Service\ItemService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Filesystem\Exception\IOException;
use Mascoteros\Application\Commands\FeedImportCommandBundle\Utils\CustomFeedXMLParser;

class FeedImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('mascoteros:feed:import')

            ->addArgument('type', InputArgument::REQUIRED, 'The type of import Product or Stock')

            ->setDescription('Feed import by type for mascoteros technical test.')

            ->setHelp("This command executes an import for hardcoded xml file into command.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type = $input->getArgument('type');

        $output->writeln([
            '<info>Loading mascoteros feed '.$type.' import...</info>',
            '',
        ]);

        /* @var FeedService $feedService */
        $feedService = $this->getContainer()->get('mascoteros.domain.service.feed');
        /* @var ItemService $itemService */
        $itemService = $this->getContainer()->get('mascoteros.domain.service.item');

        try
        {
            $xml_parser = new CustomFeedXMLParser($type);

            $feeds = $xml_parser->loadProductFeed();

            foreach ($feeds as $key_channel=>$channel)
            {
                $channel_title = $channel['title'];
                $channel_link = $channel['link'];

                $output->writeln([
                  'Importing feed: '.$channel_title.' '.$channel_link
                ]);

                $feed = $feedService->findFeedByTitle($channel_title);

                if(!$feed && $type !== 'stocks')
                {
                    $feed = $feedService->create($channel_title,$channel_link);
                }

                $items = $xml_parser->loadItemsFeed();
                $items_extra_info = $xml_parser->loadItemsExtraInfoFeed();

                foreach ($items as $key=>$item)
                {
                    $title = $item['title'];
                    $link = $item['link'];
                    $description = $item['description'];
                    //This part could be better like that,but I had some problems getting objects into
                    $img_link = $items_extra_info['image_link'][$key][0];
                    $price = (int)$items_extra_info['price'][$key][0];
                    $reduced_price = (int)$items_extra_info['reduced_price'][$key][0];
                    $ean = (int)$items_extra_info['ean'][$key][0];

                    $output->writeln([
                      $title.' '.$link.' '.$description.' '.$img_link.' '.$price.' '.$reduced_price.' '.$ean
                    ]);

                    $item = $itemService->findItemByEan($ean);
                    if($item)
                    {
                        $item->setPrice($price);
                        $item->setReducedPrice($reduced_price);
                        $itemService->update($item);
                    }
                    else
                    {
                        $itemService->create($feed,$title,$description,$link,$img_link,$price,$reduced_price,$ean);
                    }
                }
            }
        }
        catch (IOException $exception)
        {
            $output->writeln([
              $exception->getMessage()
            ]);
        }

        $output->writeln([
          '<info>Import finished...</info>',
          '',
        ]);
    }
}