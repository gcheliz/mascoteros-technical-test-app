<?php

namespace Mascoteros\Application\ApiBundle\Controller;

use Mascoteros\Domain\FeedBundle\Service\FeedService;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;

class FeedsController {
    /**
     * @var ViewHandlerInterface
     */
    private $handler;

    /**
     * @var FeedService
     */
    private $feed_service;


    public function __construct(ViewHandlerInterface $handler, FeedService $feed_service)
    {
        $this->handler = $handler;
        $this->feed_service = $feed_service;
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Get specified feed from database",
     *   requirements={
     *      {
     *          "name"="feed_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Feed Id from database"
     *      }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @param int $feed_id
     *
     * @return Response
     */
    public function getFeedAction(int $feed_id) : Response
    {
        $view = View::create(
            $this->feed_service->getFeed($feed_id),
            200
        )->setFormat('json')
            ->setHeader('Access-Control-Allow-Origin', '*');

        return $this->handler->handle($view);
    }
}
