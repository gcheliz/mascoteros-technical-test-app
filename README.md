docker-ddd-symfony
==============

Complete stack solution for symfony DDD development environment with Nginx, PHP-FPM, MySQL, RedisDB, RabbitMQ-Stomp and ELK (ElasticSearch, Logstash and Kibana)  
# Prerequisites

First of all install docker-engine script with this commmand into repository directory:

```bash
$ brew install docker
```

# Installation

First, clone this repository:

```bash
$ git clone https://github.com/gcheliz/docker-symfony-base-app.git
```

Second, do not forget to add:

* `symfony.mascoteros.com` in your `/etc/hosts` file.

Then, run:

```bash
$ docker-compose build
$ docker-compose up -d
```

Access to the PHP Docker Container

```bash
$ ./web.sh
```

Installing Symfony App
----------

Use Composer

	composer install

Create database schema (sf is an alias of php app/console)

	sf doctrine:schema:update --force
	
Execute the command

    sf mascoteros:feed:import <type> (products or stocks)
    
Execute the tests

    php bin/phpunit -c app/
    
To see the REST documentation:

    http://symfony.mascoteros.com/